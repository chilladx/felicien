#!/usr/bin/env python
# -*- coding: utf8 -*-

import copy
import pandas as pd


METRIC_NAME_PROMQL = 'up{job="prometheus", instance="localhost:9090"}'
METRIC_VECTOR = {
    "metric": {
        "__name__": "up",
        "job": "prometheus",
        "instance": "localhost:9090",
    },
    "value": [1435781430.781, "1"],
}
METRIC_MATRIX = {
    "metric": {
        "__name__": "up",
        "job": "prometheus",
        "instance": "localhost:9090",
    },
    "values": [
        [1435781430.781, "1"],
        [1435781445.781, "1"],
        [1435781460.781, "1"],
    ],
}
METRIC_VECTOR_NONAME = copy.deepcopy(METRIC_VECTOR)
METRIC_VECTOR_NONAME["metric"].pop("__name__")
METRIC_VECTOR_NOVALUE = copy.deepcopy(METRIC_VECTOR)
METRIC_VECTOR_NOVALUE.pop("value")
METRIC_VECTOR_EMPTYVALUE = copy.deepcopy(METRIC_VECTOR)
METRIC_VECTOR_EMPTYVALUE["value"] = []
METRIC_MATRIX_BADFORMATVALUE = copy.deepcopy(METRIC_MATRIX)
METRIC_MATRIX_BADFORMATVALUE["values"][1] = [1435781445.781]
METRIC_VECTOR_NOLABELS = copy.deepcopy(METRIC_VECTOR)
METRIC_VECTOR_NOLABELS["metric"].pop("job")
METRIC_VECTOR_NOLABELS["metric"].pop("instance")
DEFAULT_VALUES = pd.Series(
    data=[1, 1, 1, 1, 1],
    index=pd.to_datetime(
        [
            1435781430.781,
            1435781445.781,
            1435781460.781,
            1435781475.781,
            1435781490.781,
        ],
        unit="s",
    ),
)
DEFAULT_VALUES_BAD_FREQ_ONE_UNFREQ = pd.Series(
    data=[1, 1, 1, 1, 1],
    index=pd.to_datetime(
        [
            1435781430.781,
            1435781445.781,
            1435781460.781,
            1435781475.781,
            1435781489.781,
        ],
        unit="s",
    ),
)
DEFAULT_VALUES_BAD_FREQ_MISSING_ONE = pd.Series(
    data=[1, 1, 1, 1, 1],
    index=pd.to_datetime(
        [
            1435781430.781,
            1435781445.781,
            1435781460.781,
            1435781475.781,
            1435781505.781,
        ],
        unit="s",
    ),
)
DEFAULT_VALUES_BAD_FREQ_SLIGHT_SHIFT = pd.Series(
    data=[1, 1, 1, 1, 1],
    index=pd.to_datetime(
        [
            1435781430.781,
            1435781445.781,
            1435781460.781,
            1435781475.781,
            1435781490.782,
        ],
        unit="s",
    ),
)
DEFAULT_SINGLE_VALUE = pd.Series(
    data=[1],
    index=pd.to_datetime(
        [1435781430.781],
        unit="s",
    ),
)
DEFAULT_VALUES_FREQ_CENTRAL = pd.Series(
    data=[1, 1, 1, 1, 1],
    index=pd.to_datetime(
        [
            1435781420.781,
            1435781445.781,
            1435781460.781,
            1435781475.781,
            1435781500.781,
        ],
        unit="s",
    ),
)
DEFAULT_VALUES_FREQ_FIRST = pd.Series(
    data=[1, 1, 1, 1, 1],
    index=pd.to_datetime(
        [
            1435781430.781,
            1435781445.781,
            1435781460.781,
            1435781475.781,
            1435781500.781,
        ],
        unit="s",
    ),
)
DEFAULT_VALUES_FREQ_LAST = pd.Series(
    data=[1, 1, 1, 1, 1],
    index=pd.to_datetime(
        [
            1435781420.781,
            1435781445.781,
            1435781460.781,
            1435781475.781,
            1435781490.781,
        ],
        unit="s",
    ),
)
DEFAULT_VALUES_TWO_SEGMENTS = pd.Series(
    data=[1, 1, 1, 1, 1, 1, 1],
    index=pd.to_datetime(
        [
            1435781430.781,
            1435781445.781,
            1435781460.781,
            1435781480.781,
            1435781500.781,
            1435781515.781,
            1435781530.781,
        ],
        unit="s",
    ),
)
DEFAULT_VALUES_TWO_SEGMENTS_DIFFERENT_LENGTH = pd.Series(
    data=[1, 1, 1, 1, 1, 1, 1, 1],
    index=pd.to_datetime(
        [
            1435781430.781,
            1435781445.781,
            1435781460.781,
            1435781480.781,
            1435781500.781,
            1435781515.781,
            1435781530.781,
            1435781545.781,
        ],
        unit="s",
    ),
)

DEFAULT_URL = "https://gitlab.com"
API_RESPONSE_HEALTH = {"status": "success"}
API_RESPONSE_VECTOR = {
    "status": "success",
    "data": {"resultType": "vector", "result": [METRIC_VECTOR]},
}
API_RESPONSE_MATRIX = {
    "status": "success",
    "data": {"resultType": "matrix", "result": [METRIC_MATRIX]},
}
API_RESPONSE_BAD_STATUS = {
    "status": "error",
    "data": {"resultType": "vector", "result": [METRIC_VECTOR]},
}
API_RESPONSE_MANY_RESULT = {
    "status": "success",
    "data": {
        "resultType": "vector",
        "result": [
            METRIC_VECTOR,
            METRIC_VECTOR,
        ],
    },
}
API_RESPONSE_NO_RESULT = {
    "status": "success",
    "data": {"resultType": "vector", "result": []},
}
API_RESPONSE_SCALAR = {
    "status": "success",
    "data": {"resultType": "scalar", "result": [METRIC_VECTOR]},
}

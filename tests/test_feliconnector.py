#!/usr/bin/env python
# -*- coding: utf8 -*-

import pytest
from tests import fixtures
import re
from felicien.feliconnector import FeliConnector
from felicien.felits import FeliTS

MOCK_DEFAULT_URL = re.compile(fixtures.DEFAULT_URL)


@pytest.mark.parametrize(
    "test_input,expected", [(fixtures.DEFAULT_URL, FeliConnector)]
)
def test_feliconnector_init_url(test_input, expected, requests_mock):
    requests_mock.get(MOCK_DEFAULT_URL, json=fixtures.API_RESPONSE_HEALTH)
    assert isinstance(FeliConnector(url=test_input), expected)


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (
            ("foo"),
            pytest.raises(ValueError),
        ),
        (
            ("https://gitlab.com"),
            pytest.raises(ConnectionError),
        ),
    ],
)
def test_feliconnector_init_url_exception(test_input, expected):
    with expected:
        _ = FeliConnector(url=test_input)


@pytest.mark.parametrize(
    "test_input,expected",
    [("prometheus", FeliConnector), ("victoriametrics", FeliConnector)],
)
def test_feliconnector_init_tsdb(test_input, expected, requests_mock):
    requests_mock.get(MOCK_DEFAULT_URL, json=fixtures.API_RESPONSE_HEALTH)
    assert isinstance(
        FeliConnector(url=fixtures.DEFAULT_URL, tsdb=test_input), expected
    )


@pytest.mark.parametrize(
    "test_input,expected",
    [
        ("foo", pytest.raises(ValueError)),
        (None, pytest.raises(ValueError)),
    ],
)
def test_feliconnector_init_tsdb_exception(test_input, expected):
    with expected:
        _ = FeliConnector(url=fixtures.DEFAULT_URL, tsdb=test_input)


@pytest.mark.parametrize(
    "test_input",
    [
        {"auth": ("foo", "bar")},
        {"verify": True},
        {"verify": "cert.crt"},
        {"cert": ("cert.crt", "cert.key")},
        {"cert": "cert.crt"},
        {"headers": {"user-agent": "my-app/0.0.1"}},
    ],
)
def test_feliconnector_init_options(test_input, requests_mock):
    requests_mock.get(MOCK_DEFAULT_URL, json=fixtures.API_RESPONSE_HEALTH)
    res = FeliConnector(url=fixtures.DEFAULT_URL, options=test_input)
    for k, v in res._options.items():
        assert k in test_input.keys()
        assert v == test_input[k]


@pytest.mark.parametrize(
    "test_input,expected",
    [
        ({"auth": True}, pytest.raises(KeyError)),
        ({"verify": list()}, pytest.raises(KeyError)),
        ({"foo": "cert.crt"}, pytest.raises(KeyError)),
        ({"headers": "custom"}, pytest.raises(KeyError)),
    ],
)
def test_feliconnector_init_options_exception(test_input, expected):
    with expected:
        _ = FeliConnector(url=fixtures.DEFAULT_URL, options=test_input)


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (fixtures.API_RESPONSE_VECTOR, 1),
        (
            fixtures.API_RESPONSE_MATRIX,
            len(
                fixtures.API_RESPONSE_MATRIX["data"]["result"][0].get("values")
            ),
        ),
    ],
)
def test_feliconnector_get_timeserie(test_input, expected, requests_mock):
    requests_mock.get(MOCK_DEFAULT_URL, json=fixtures.API_RESPONSE_HEALTH)
    tsdb = FeliConnector(url=fixtures.DEFAULT_URL)
    requests_mock.get(MOCK_DEFAULT_URL, json=test_input)
    res = tsdb.get_timeserie(metric="up")

    assert res.size == expected


@pytest.mark.parametrize(
    "test_input,status_code,expected",
    [
        (fixtures.API_RESPONSE_VECTOR, 201, pytest.raises(ConnectionError)),
        (
            fixtures.API_RESPONSE_BAD_STATUS,
            200,
            pytest.raises(ConnectionError),
        ),
        (fixtures.API_RESPONSE_MANY_RESULT, 200, pytest.raises(OverflowError)),
        (fixtures.API_RESPONSE_NO_RESULT, 200, pytest.raises(ValueError)),
        (fixtures.API_RESPONSE_SCALAR, 200, pytest.raises(TypeError)),
    ],
)
def test_feliconnector_get_timeserie_exception(
    test_input, status_code, expected, requests_mock
):
    requests_mock.get(MOCK_DEFAULT_URL, json=fixtures.API_RESPONSE_HEALTH)
    tsdb = FeliConnector(url=fixtures.DEFAULT_URL)
    with expected:
        requests_mock.get(
            MOCK_DEFAULT_URL, json=test_input, status_code=status_code
        )
        _ = tsdb.get_timeserie(metric="up")


@pytest.mark.parametrize(
    "test_input, test_tsdb, test_method, expected",
    [
        (fixtures.METRIC_NAME_PROMQL, "prometheus", "post", True),
        (fixtures.METRIC_NAME_PROMQL, "victoriametrics", "get", True),
    ],
)
def test_feliconnector_delete_timeserie(
    test_input, test_tsdb, test_method, expected, requests_mock
):
    requests_mock.get(MOCK_DEFAULT_URL, json=fixtures.API_RESPONSE_HEALTH)
    tsdb = FeliConnector(url=fixtures.DEFAULT_URL, tsdb=test_tsdb)
    requests_mock.request(
        method=test_method, url=MOCK_DEFAULT_URL, status_code=204
    )
    assert tsdb.delete_timeserie(metric=test_input) is expected


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (fixtures.METRIC_NAME_PROMQL, pytest.raises(ConnectionError)),
    ],
)
def test_feliconnector_delete_timeserie_exception(
    test_input, expected, requests_mock
):
    requests_mock.get(MOCK_DEFAULT_URL, json=fixtures.API_RESPONSE_HEALTH)
    tsdb = FeliConnector(url=fixtures.DEFAULT_URL)
    requests_mock.post(MOCK_DEFAULT_URL, status_code=200)
    with expected:
        _ = tsdb.delete_timeserie(metric=test_input)


@pytest.mark.parametrize(
    "test_input, test_tsdb, expected",
    [
        (fixtures.METRIC_MATRIX, "prometheus", True),
        (fixtures.METRIC_MATRIX, "victoriametrics", True),
    ],
)
def test_feliconnector_import_timeserie(
    test_input, test_tsdb, expected, requests_mock
):
    requests_mock.get(MOCK_DEFAULT_URL, json=fixtures.API_RESPONSE_HEALTH)
    tsdb = FeliConnector(url=fixtures.DEFAULT_URL, tsdb=test_tsdb)
    requests_mock.post(url=MOCK_DEFAULT_URL)
    assert tsdb.import_timeserie(ts=FeliTS(from_prom=test_input)) is expected


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (fixtures.METRIC_MATRIX, pytest.raises(ConnectionError)),
    ],
)
def test_feliconnector_import_timeserie_exception(
    test_input, expected, requests_mock
):
    requests_mock.get(MOCK_DEFAULT_URL, json=fixtures.API_RESPONSE_HEALTH)
    tsdb = FeliConnector(url=fixtures.DEFAULT_URL)
    requests_mock.post(MOCK_DEFAULT_URL, status_code=500)
    with expected:
        _ = tsdb.import_timeserie(ts=FeliTS(from_prom=test_input))

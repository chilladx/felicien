#!/usr/bin/env python
# -*- coding: utf8 -*-

import pytest
import pandas as pd
import datetime as dt
import json
from tests import fixtures
from felicien.felits import FeliTS


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (
            fixtures.METRIC_VECTOR_NONAME,
            pytest.raises(AttributeError),
        ),
        (
            fixtures.METRIC_VECTOR_NOVALUE,
            pytest.raises(AttributeError),
        ),
        (
            fixtures.METRIC_VECTOR_EMPTYVALUE,
            pytest.raises(ValueError),
        ),
        (
            fixtures.METRIC_MATRIX_BADFORMATVALUE,
            pytest.raises(ValueError),
        ),
    ],
)
def test_felits_init_fromprom_exceptions(test_input, expected):
    with expected:
        _ = FeliTS(from_prom=test_input)


@pytest.mark.parametrize(
    "input_name, input_labels, input_value,expected",
    [
        (
            "",
            dict(),
            fixtures.DEFAULT_VALUES,
            pytest.raises(AttributeError),
        ),
        (
            "foo",
            dict(),
            None,
            pytest.raises(AttributeError),
        ),
        (
            "foo",
            dict(),
            pd.Series(),
            pytest.raises(ValueError),
        ),
    ],
)
def test_felits_init_rawdata_exceptions(
    input_name, input_labels, input_value, expected
):
    with expected:
        _ = FeliTS(name=input_name, labels=input_labels, values=input_value)


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (
            None,
            pytest.raises(AttributeError),
        ),
    ],
)
def test_felits_init_noparam_exceptions(test_input, expected):
    with expected:
        _ = FeliTS(test_input)


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (
            fixtures.METRIC_VECTOR,
            fixtures.METRIC_VECTOR.get("metric").get("__name__"),
        ),
        (
            fixtures.METRIC_MATRIX,
            fixtures.METRIC_MATRIX.get("metric").get("__name__"),
        ),
    ],
)
def test_felits_init_fromprom_name(test_input, expected):
    assert FeliTS(from_prom=test_input).name == expected


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (
            fixtures.METRIC_VECTOR,
            fixtures.METRIC_VECTOR.get("metric").get("__name__"),
        ),
        (
            fixtures.METRIC_MATRIX,
            fixtures.METRIC_MATRIX.get("metric").get("__name__"),
        ),
    ],
)
def test_felits_init_rawdata_name(test_input, expected):
    assert (
        FeliTS(
            name=test_input.get("metric").get("__name__"),
            values=fixtures.DEFAULT_VALUES,
        ).name
        == expected
    )


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (
            fixtures.METRIC_VECTOR,
            'job="prometheus", instance="localhost:9090"',
        ),
        (
            fixtures.METRIC_MATRIX,
            'job="prometheus", instance="localhost:9090"',
        ),
        (
            fixtures.METRIC_VECTOR_NOLABELS,
            "",
        ),
    ],
)
def test_felits_labels_string(test_input, expected):
    assert FeliTS(from_prom=test_input).labels_string == expected


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (fixtures.DEFAULT_VALUES, fixtures.DEFAULT_VALUES.size),
        (fixtures.DEFAULT_VALUES.iloc[[0]], 1),
    ],
)
def test_felits_size(test_input, expected):
    assert FeliTS(name="foo", values=test_input).size == expected


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (
            fixtures.METRIC_VECTOR,
            fixtures.METRIC_VECTOR.get("metric").get("__name__"),
        ),
        (
            fixtures.METRIC_MATRIX,
            fixtures.METRIC_MATRIX.get("metric").get("__name__"),
        ),
        (
            fixtures.METRIC_VECTOR_NOLABELS,
            fixtures.METRIC_VECTOR_NOLABELS.get("metric").get("__name__"),
        ),
    ],
)
def test_felits_as_prometheus(test_input, expected):
    res = FeliTS(from_prom=test_input).as_prometheus()
    assert isinstance(res, str)
    assert json.loads(res).get("metric").get("__name__") == expected
    assert len(json.loads(res).get("values")) == len(
        json.loads(res).get("timestamps")
    )


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (
            fixtures.METRIC_VECTOR,
            fixtures.METRIC_VECTOR.get("metric").get("__name__"),
        ),
        (
            fixtures.METRIC_MATRIX,
            fixtures.METRIC_MATRIX.get("metric").get("__name__"),
        ),
        (
            fixtures.METRIC_VECTOR_NOLABELS,
            fixtures.METRIC_VECTOR_NOLABELS.get("metric").get("__name__"),
        ),
    ],
)
def test_felits_as_dict(test_input, expected):
    res = FeliTS(from_prom=test_input).as_dict()
    assert isinstance(res, dict)
    assert res.get("metric").get("__name__") == expected
    assert len(res.get("values")) == len(res.get("timestamps"))


@pytest.mark.parametrize(
    "test_input,test_format,expected",
    [
        (
            fixtures.METRIC_VECTOR,
            "s",
            fixtures.METRIC_VECTOR["value"][0],
        ),
        (
            fixtures.METRIC_VECTOR,
            "ms",
            fixtures.METRIC_VECTOR["value"][0] * 1000,
        ),
    ],
)
def test_felits_as_dict_timestamp_format(test_input, test_format, expected):
    res = FeliTS(from_prom=test_input).as_dict(timestamp_format=test_format)
    assert res.get("timestamps")[0] == int(expected)


@pytest.mark.parametrize(
    "test_input,test_name,expected, expected_size",
    [
        (
            fixtures.METRIC_VECTOR,
            "",
            [fixtures.METRIC_VECTOR.get("metric").get("__name__")],
            1,
        ),
        (
            fixtures.METRIC_MATRIX,
            "foo",
            ["foo"],
            len(fixtures.METRIC_MATRIX.get("values")),
        ),
    ],
)
def test_felits_as_dataframe(test_input, test_name, expected, expected_size):
    res = FeliTS(from_prom=test_input).as_dataframe(name=test_name)
    assert isinstance(res, pd.DataFrame)
    assert list(res.columns) == expected
    assert res.size == expected_size


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (
            pd.Series(
                data=[1, 1, 1, 1, 1],
                index=pd.to_datetime(
                    [
                        1435781430.781,
                        1435781445.781,
                        1435781460.781,
                        1435781475.781,
                        1435781490.781,
                    ],
                    unit="s",
                ),
            ),
            dt.timedelta(seconds=15),
        ),
        (
            pd.Series(
                data=[1, 1, 1, 1, 1],
                index=pd.to_datetime(
                    [
                        1435781430.781,
                        1435781445.781,
                        1435781460.781,
                        1435781475.781,
                        1435781480.781,
                    ],
                    unit="s",
                ),
            ),
            dt.timedelta(seconds=15),
        ),
        (
            pd.Series(
                data=[1, 1, 1, 1, 1, 1],
                index=pd.to_datetime(
                    [
                        1435781440.781,
                        1435781445.781,
                        1435781460.781,
                        1435781475.781,
                        1435781480.781,
                        1435781500.781,
                    ],
                    unit="s",
                ),
            ),
            dt.timedelta(seconds=5),
        ),
        (
            pd.Series(
                data=[1],
                index=pd.to_datetime(
                    [1435781440.781],
                    unit="s",
                ),
            ),
            dt.timedelta(0),
        ),
        (
            pd.Series(
                data=[1, 1],
                index=pd.to_datetime(
                    [
                        1435781440.781,
                        1435781455.781,
                    ],
                    unit="s",
                ),
            ),
            dt.timedelta(seconds=15),
        ),
        (
            pd.Series(
                data=[1, 1, 1],
                index=pd.to_datetime(
                    [
                        1435781440.781,
                        1435781445.781,
                        1435781460.781,
                    ],
                    unit="s",
                ),
            ),
            dt.timedelta(seconds=5),
        ),
        (
            pd.Series(
                data=[1, 1, 1, 1, 1],
                index=pd.to_datetime(
                    [
                        1435781420.781,
                        1435781445.781,
                        1435781460.781,
                        1435781475.781,
                        1435781500.781,
                    ],
                    unit="s",
                ),
            ),
            dt.timedelta(seconds=15),
        ),
    ],
)
def test_felits_frequency(test_input, expected):
    assert FeliTS(name="foo", values=test_input).frequency == expected


@pytest.mark.parametrize(
    "input_values, input_boundary, input_keep, expected",
    [
        (
            fixtures.DEFAULT_VALUES,
            fixtures.DEFAULT_VALUES.index[1],
            "right",
            fixtures.DEFAULT_VALUES.size - 1,
        ),
        (
            fixtures.DEFAULT_VALUES,
            fixtures.DEFAULT_VALUES.index[1],
            "left",
            2,
        ),
        (
            fixtures.DEFAULT_VALUES.iloc[[0]],
            fixtures.DEFAULT_VALUES.iloc[[0]].index[0],
            "left",
            1,
        ),
        (
            fixtures.DEFAULT_VALUES.iloc[[0]],
            fixtures.DEFAULT_VALUES.iloc[[0]].index[0],
            "right",
            1,
        ),
    ],
)
def test_felits_trim_by_date(
    input_values, input_boundary, input_keep, expected
):
    res = FeliTS(name="foo", values=input_values).trim_by_date(
        boundary=input_boundary, keep=input_keep
    )
    assert res.size == expected


@pytest.mark.parametrize(
    "input_values, input_boundary, input_keep, expected",
    [
        (fixtures.DEFAULT_VALUES, 2, "right", 2),
        (fixtures.DEFAULT_VALUES, 2, "left", 2),
        (
            fixtures.DEFAULT_VALUES,
            fixtures.DEFAULT_VALUES.size + 1,
            "right",
            fixtures.DEFAULT_VALUES.size,
        ),
        (
            fixtures.DEFAULT_VALUES,
            fixtures.DEFAULT_VALUES.size + 1,
            "left",
            fixtures.DEFAULT_VALUES.size,
        ),
        (fixtures.DEFAULT_VALUES, 0, "right", fixtures.DEFAULT_VALUES.size),
        (fixtures.DEFAULT_VALUES, 0, "left", fixtures.DEFAULT_VALUES.size),
    ],
)
def test_felits_trim_by_size(
    input_values, input_boundary, input_keep, expected
):
    res = FeliTS(name="foo", values=input_values).trim_by_size(
        boundary=input_boundary, keep=input_keep
    )
    assert res.size == expected


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (fixtures.DEFAULT_VALUES, fixtures.DEFAULT_VALUES.size),
        (
            fixtures.DEFAULT_VALUES_BAD_FREQ_ONE_UNFREQ,
            fixtures.DEFAULT_VALUES_BAD_FREQ_ONE_UNFREQ.size - 1,
        ),
        (
            fixtures.DEFAULT_VALUES_BAD_FREQ_MISSING_ONE,
            fixtures.DEFAULT_VALUES_BAD_FREQ_MISSING_ONE.size + 1,
        ),
    ],
)
def test_felits_normalize(test_input, expected):
    res = FeliTS(name="foo", values=test_input).normalize()
    assert res.size == expected


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (fixtures.DEFAULT_VALUES, fixtures.DEFAULT_VALUES.size),
    ],
)
def test_felits_normalize_inplace(test_input, expected):
    res = FeliTS(name="foo", values=test_input)
    res.normalize(inplace=True)
    assert res.size == expected


@pytest.mark.parametrize(
    "test_input,expected",
    [
        (fixtures.DEFAULT_SINGLE_VALUE, pytest.raises(ValueError)),
    ],
)
def test_felits_normalize_exception(test_input, expected):
    res = FeliTS(name="foo", values=test_input)
    with expected:
        _ = res.normalize()


@pytest.mark.parametrize(
    "test_input,pos,exp_size, exp_ts",
    [
        (
            fixtures.DEFAULT_VALUES,
            "first",
            fixtures.DEFAULT_VALUES.size,
            fixtures.DEFAULT_VALUES.index[0],
        ),
        (
            fixtures.DEFAULT_VALUES,
            "last",
            fixtures.DEFAULT_VALUES.size,
            fixtures.DEFAULT_VALUES.index[0],
        ),
        (
            fixtures.DEFAULT_VALUES_BAD_FREQ_MISSING_ONE,
            "first",
            4,
            fixtures.DEFAULT_VALUES_BAD_FREQ_MISSING_ONE.index[0],
        ),
        (
            fixtures.DEFAULT_SINGLE_VALUE,
            "first",
            1,
            fixtures.DEFAULT_SINGLE_VALUE.index[0],
        ),
        (
            fixtures.DEFAULT_VALUES_FREQ_CENTRAL,
            "first",
            3,
            fixtures.DEFAULT_VALUES_FREQ_CENTRAL.index[1],
        ),
        (
            fixtures.DEFAULT_VALUES_FREQ_CENTRAL,
            "last",
            3,
            fixtures.DEFAULT_VALUES_FREQ_CENTRAL.index[1],
        ),
        (
            fixtures.DEFAULT_VALUES_FREQ_FIRST,
            "first",
            4,
            fixtures.DEFAULT_VALUES_FREQ_FIRST.index[0],
        ),
        (
            fixtures.DEFAULT_VALUES_FREQ_FIRST,
            "last",
            4,
            fixtures.DEFAULT_VALUES_FREQ_FIRST.index[0],
        ),
        (
            fixtures.DEFAULT_VALUES_FREQ_LAST,
            "first",
            4,
            fixtures.DEFAULT_VALUES_FREQ_LAST.index[1],
        ),
        (
            fixtures.DEFAULT_VALUES_FREQ_LAST,
            "last",
            4,
            fixtures.DEFAULT_VALUES_FREQ_LAST.index[1],
        ),
        (
            fixtures.DEFAULT_VALUES_TWO_SEGMENTS,
            "first",
            3,
            fixtures.DEFAULT_VALUES_TWO_SEGMENTS.index[0],
        ),
        (
            fixtures.DEFAULT_VALUES_TWO_SEGMENTS,
            "last",
            3,
            fixtures.DEFAULT_VALUES_TWO_SEGMENTS.index[4],
        ),
    ],
)
def test_felits_longest_continuous_segment(test_input, pos, exp_size, exp_ts):
    res = FeliTS(name="foo", values=test_input).continuous_segment(
        position=pos, longest=True
    )
    assert res.size == exp_size
    assert res.index[0] == exp_ts


@pytest.mark.parametrize(
    "test_longest,test_pos,exp_size, exp_ts",
    [
        (
            True,
            "last",
            4,
            fixtures.DEFAULT_VALUES_TWO_SEGMENTS_DIFFERENT_LENGTH.index[4],
        ),
        (
            True,
            "first",
            4,
            fixtures.DEFAULT_VALUES_TWO_SEGMENTS_DIFFERENT_LENGTH.index[4],
        ),
        (
            False,
            "last",
            4,
            fixtures.DEFAULT_VALUES_TWO_SEGMENTS_DIFFERENT_LENGTH.index[4],
        ),
        (
            False,
            "first",
            3,
            fixtures.DEFAULT_VALUES_TWO_SEGMENTS_DIFFERENT_LENGTH.index[0],
        ),
    ],
)
def test_felits_continuous_segment(test_longest, test_pos, exp_size, exp_ts):
    res = FeliTS(
        name="foo",
        values=fixtures.DEFAULT_VALUES_TWO_SEGMENTS_DIFFERENT_LENGTH,
    ).continuous_segment(position=test_pos, longest=test_longest)
    assert res.size == exp_size
    assert res.index[0] == exp_ts


@pytest.mark.parametrize(
    "test_pos, expected",
    [
        (
            "foo",
            pytest.raises(ValueError),
        ),
        (
            42,
            pytest.raises(ValueError),
        ),
        (
            True,
            pytest.raises(ValueError),
        ),
    ],
)
def test_felits_continuous_segment_exception(test_pos, expected):
    res = FeliTS(
        name="foo",
        values=fixtures.DEFAULT_VALUES_TWO_SEGMENTS_DIFFERENT_LENGTH,
    )
    with expected:
        _ = res.continuous_segment(position=test_pos)

ARG PYTHON_VERSION=3.11

FROM python:$PYTHON_VERSION

COPY pyproject.toml poetry.lock /

RUN pip install --no-cache-dir 'poetry==1.6.1'

ENV POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_IN_PROJECT=0 \
    POETRY_VIRTUALENVS_PATH=/venv

RUN poetry install --with dev --no-root

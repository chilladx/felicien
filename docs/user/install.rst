.. _install:

Installation
============

This part of the documentation covers the installation of Felicien.
The first step to using any software package is getting it properly installed.


$ pip install felicien
----------------------

Felicien package is hosted on PyPI, so you can install it with the following command::

    $ pip install felicien

Get the Source Code
-------------------

Felicien is actively developed on GitLab, where the code is
`available <https://gitlab.com/chilladx/felicien/>`_.

You can either clone the public repository::

    $ git clone https://gitlab.com/chilladx/felicien.git

Or, download the `tarball <https://gitlab.com/chilladx/felicien/-/archive/main/felicien-main.tar.gz>`_::

    $ curl -OL https://gitlab.com/chilladx/felicien/-/archive/main/felicien-main.tar.gz
    # optionally, zipball is also available (for Windows users).

Once you have a copy of the source, you can embed it in your own Python
package, or install it into your site-packages easily::

    $ cd felicien
    $ python -m pip install .
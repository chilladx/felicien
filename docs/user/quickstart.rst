.. _quickstart:

Quickstart
==========

First thing you need to do is to :ref:`install <install>` Felicien.

Let's play with it, now.

Connect to a TSDB
-----------------

You need to import the FeliConnector module::

    >>> from felicien import FeliConnector

Now let's connect to a TSDB::

    >>> tsdb = FeliConnector(url="http://victoriametrics:8428", tsdb="victoriametrics")

You have a :class:`FeliConnector <felicien.FeliConnector>` object called ``tsdb``::

    >>> tsdb
    FeliConnector([victoriametrics]{http://victoriametrics:8428})

You can use it to interact with the TSDB.

Get data from the TSDB
----------------------

A timeserie is a representation of a metric from Prometheus/VictoriaMetrics::

    >>> my_metric = 'vm_cache_entries{job=~"victoriametrics", instance=~"victoriametrics:8428", type="storage/hour_metric_ids"}'

Use your :class:`FeliConnector <felicien.FeliConnector>` object to get a single value metric::

    >>> ts_scalar = tsdb.get_timeserie(metric=f"{my_metric}")
    >>> ts_scalar
    FeliTS(vm_cache_entries{instance:"victoriametrics:8428", job:"victoriametrics", type:"storage/hour_metric_ids"}, 1 datapoints)

Same applies for a longer timeserie::

    >>> ts_vector = tsdb.get_timeserie(metric=f"{my_metric}[1h]")
    >>> ts_vector
    FeliTS(vm_cache_entries{job:"victoriametrics", type:"storage/hour_metric_ids", instance:"victoriametrics:8428"}, 60 datapoints)

The timeserie object
--------------------

A :class:`FeliTS <felicien.FeliTS>` object is a representation of a timeserie from Prometheus/VictoriaMetrics. It's usually a metric name with labels, and a serie of values indexed by time.

You can extract information about the serie, such as its frequency::

    >>> ts_vector.frequency
    Timedelta('0 days 00:01:00')

Or its size::

    >>> ts_vector.size
    60

You can also view it as a `Prometheus JSON line format <https://docs.victoriametrics.com/#json-line-format>`_ representation::

    >>> ts_scalar.as_prometheus()
    {'metric': {'__name__': 'vm_cache_entries',
    'instance': 'victoriametrics:8428',
    'job': 'victoriametrics',
    'type': 'storage/hour_metric_ids'},
    'values': [17785.0],
    'timestamps': [1713739511000]}

Or as a `Pandas DataFrame <https://pandas.pydata.org/pandas-docs/stable/reference/frame.html>`_ format::

    >>> ts_scalar.as_dataframe()
                         vm_cache_entries
    2024-04-21 22:45:11           17785.0

The timeserie can also be truncated, either by size::

    >>> ts_vector.size
    60
    >>> ts_vector.trim_by_size(boundary=10, keep="left").size
    10

Or by date::

    >>> ts_vector.trim_by_date(boundary=ts_vector.data.index[12], keep="left").size
    13

-----------------------

Want more? Check out the :ref:`advanced <advanced>` section.
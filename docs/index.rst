.. Felicien documentation master file, created by
   sphinx-quickstart on Sun Apr 21 16:08:39 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Felicien's documentation!
====================================


.. image:: https://gitlab.com/chilladx/felicien/-/badges/release.svg
    :target: https://gitlab.com/chilladx/felicien/-/releases
    :alt: Felicien latest release
    
.. image:: https://static.pepy.tech/badge/felicien/month
    :target: https://pepy.tech/project/felicien
    :alt: Felicien Downloads Per Month Badge
    
.. image:: https://img.shields.io/pypi/l/felicien.svg
    :target: https://pypi.org/project/felicien/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/felicien.svg
    :target: https://pypi.org/project/felicien/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/felicien.svg
    :target: https://pypi.org/project/felicien/
    :alt: Python Version Support Badge

**Felicien** is you companion to retrieve timeseries from a TSDB, to transform it in various format and to push it to a TSDB. Supported TSDB are Prometheus compatible (Prometheus, VictoriaMetrics, ...).

------

::
    
   >>> from felicien import FeliConnector
   >>> tsdb = FeliConnector(url="https://my.victoriametrics.instance", tsdb="victoriametrics")
   >>> tsdb
   FeliConnector([victoriametrics]{https://my.victoriametrics.instance})
   
   >>> ts_scalar = tsdb.get_timeserie(metric='vm_cache_entries{job=~"victoriametrics", instance=~"victoriametrics:8428", type="storage/hour_metric_ids"}')
   >>> ts_scalar
   FeliTS(vm_cache_entries{instance:"victoriametrics:8428", job:"victoriametrics", type:"storage/hour_metric_ids"}, 1 datapoints)
   >>> ts_scalar.as_prometheus()
   {'metric': {'__name__': 'vm_cache_entries',
   'instance': 'victoriametrics:8428',
   'job': 'victoriametrics',
   'type': 'storage/hour_metric_ids'},
   'values': [17805.0],
   'timestamps': [1713606731000]}
   
   >>> ts_vector = tsdb.get_timeserie(metric='vm_cache_entries{job=~"victoriametrics", instance=~"victoriametrics:8428", type="storage/hour_metric_ids"}[1h]')
   >>> ts_vector
   FeliTS(vm_cache_entries{job:"victoriametrics", type:"storage/hour_metric_ids", instance:"victoriametrics:8428"}, 60 datapoints)
   >>> ts_vector.frequency
   Timedelta('0 days 00:01:00')
   >>> ts_vector.data.describe()
   count       60.000000
   mean     17768.150000
   std          5.580915
   min      17766.000000
   25%      17766.000000
   50%      17766.000000
   75%      17767.000000
   max      17805.000000
   dtype: float64
   >>> ts_vector.trim_by_size(boundary=10, keep="left")
   2024-04-20 09:03:40.177000046    17766.0
   2024-04-20 09:04:40.177000046    17766.0
   2024-04-20 09:05:40.177000046    17766.0
   2024-04-20 09:06:40.177000046    17766.0
   2024-04-20 09:07:40.177000046    17766.0
   2024-04-20 09:08:40.177000046    17766.0
   2024-04-20 09:09:40.177000046    17766.0
   2024-04-20 09:10:40.177000046    17766.0
   2024-04-20 09:11:40.177000046    17766.0
   2024-04-20 09:12:40.177000046    17766.0
   dtype: float64


Main features
-------------

- Connect to a TSDB, and check connectivity
- Get a timeserie and store it in a Pandas Series
- Estimate frequency of a timeserie
- Trim a timeserie by date or by size
- Transform the timeserie in a pandas.DataFrame
- Delete a timeserie in a TSDB
- Import a timeserie into a TSDB


User Guide
----------

.. toctree::
   :maxdepth: 2

   user/install
   user/quickstart
   user/advanced


The API Documentation / Guide
-----------------------------

.. toctree::
   :maxdepth: 2

   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`

.. _api:

Developer Interface
===================

.. module:: felicien

.. autosummary::

FeliConnector
-------------

.. _feliconnector:

.. autoclass:: FeliConnector
   :inherited-members:

   .. automethod:: __init__

FeliTS
------

.. _felits:

.. autoclass:: FeliTS
   :inherited-members:

   .. automethod:: __init__
